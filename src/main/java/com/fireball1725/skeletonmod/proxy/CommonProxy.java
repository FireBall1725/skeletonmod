package com.fireball1725.skeletonmod.proxy;

import com.fireball1725.skeletonmod.block.Blocks;
import com.fireball1725.skeletonmod.item.Items;

public abstract class CommonProxy implements IProxy {
    public void registerBlocks() {
        Blocks.registerAll();
    }

    public void registerItems() {
        Items.registerAll();
    }
}
